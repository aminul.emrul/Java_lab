/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loop;

/**
 *
 * @author Emrul
 */
public class LoopClass {

    int row, numberOfStars;

    public void loopFor() {
        for (row = 1; row <= 10; row++) {
            for (numberOfStars = 1; numberOfStars <= row; numberOfStars++) {
                System.out.print("*");
            }
            System.out.println(); // Go to next line
        }
    }
}
