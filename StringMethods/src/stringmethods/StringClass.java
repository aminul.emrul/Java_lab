/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringmethods;

/**
 *
 * @author Emrul
 */
public class StringClass {

    int n;
    String s = "Java programming", t = "", u = "";

    public void lengthOfString() {
        n = s.length();
        System.out.println("Number of characters = " + n);
    }

    public void ReplaceCharacters() {
        t = s.replace("Java", "C++");
        System.out.println(s);
        System.out.println(t);
    }

    public void ConcatenatingString() {
        u = s.concat(" is fun");
        System.out.println(s);
        System.out.println(u);
    }
}
