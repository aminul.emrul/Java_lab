/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whileloop;

import java.util.Scanner;

/**
 *
 * @author Emrul
 */
public class WhileLoopClass {

    int n;

    public void Whileloop() {
        Scanner input = new Scanner(System.in);
        System.out.println("Input an integer: ");
        while ((n = input.nextInt()) != 0) {
            System.out.println("You entered " + n);
            System.out.println("Input an integer");
        }
        System.out.println("Out of loop");
        
    }
}
